from setuptools import setup, find_packages
setup(
    name = "myinfomonitor-zencoder-converter",
    version = "1.0.10",
    packages = find_packages(),
    scripts = ['encodevideo.py'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = ['boto', 'zencoder', 'filechunkio', 'pytz'],

    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        'hello': ['*.msg'],
    },

    # metadata for upload to PyPI
    author = "myinfomonitor",
    author_email = "myinfomonitor@myinfomonitor.com",
    description = "Quick and dirty Zencoder+s3 video encoding runner",
    license = "MIT",
    keywords = "",
    url = "http://myinfomonitor.com/"

)