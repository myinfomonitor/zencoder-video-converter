import ConfigParser
from datetime import datetime
import json
from optparse import OptionParser
import os
import traceback
from boto.exception import S3ResponseError
import hashlib
import math
import pytz
from zencoder import Zencoder
from boto.s3.connection import S3Connection, boto
import sys
from filechunkio import FileChunkIO



def convert_file(src_file):
    src_file_ext = os.path.splitext(src_file)[1]
    src_size = os.stat(src_file).st_size
    if src_size is 0:
        print "ERROR: File \"{0}\" is zero-length!".format(src_file)
        return

    h = hashlib.md5()
    h.update(src_file)
    filename_hash = h.hexdigest()
    s3inputfile = "i/{0}/{1}{2}".format(config.get("application", "subdir"), filename_hash, src_file_ext)
    if options.gethash:
        print s3inputfile
        sys.exit(0)

    s3outputfile = "o/{0}/{1}.{2}".format(config.get("application", "subdir"),
                                          filename_hash,
                                          config.get("output", "file_extension"))
    bucket_name = config.get("s3", "bucket")


    s3_conn = S3Connection(config.get("s3", "accessKey"), config.get("s3", "secret"),
                           host=config.get("s3", "regionalEndpoint"))

    bucket = s3_conn.get_bucket(bucket_name, validate=False)

    should_encode = False
    should_upload = False

    src_file_modified = datetime.fromtimestamp(os.path.getmtime(src_file), tz=pytz.UTC)
    print "Checking file \"{0}\" ({1},{2})".format(src_file, s3inputfile, s3outputfile)
    try:
        k = bucket.lookup(s3inputfile)
        if k is not None:
            s3_file_modified = boto.utils.parse_ts(k.last_modified).replace(tzinfo=pytz.UTC)
            should_encode = should_upload = (s3_file_modified < src_file_modified or src_size != k.size)
        else:
            should_encode = True
            should_upload = True

        if options.testtarget:
            try:
                k = bucket.lookup(s3outputfile)
                if k is not None:
                    s3_file_modified = boto.utils.parse_ts(k.last_modified).replace(tzinfo=pytz.UTC)
                    should_encode = should_encode or (s3_file_modified < src_file_modified)
                else:
                    should_encode = True
            except S3ResponseError:
                print "target file not found {0}".format(s3outputfile)
                should_encode = True

    except S3ResponseError:
        should_upload = True

    if should_upload is True:
        print "Uploading file to S3...".format(src_file)

        mp = bucket.initiate_multipart_upload(s3inputfile)
        chunk_size = 52428800
        chunk_count = int(math.ceil(src_size / float(chunk_size)))
        for i in range(chunk_count):
            offset = chunk_size * i
            bytes = min(chunk_size, src_size - offset)
            with FileChunkIO(src_file, "r", offset=offset, bytes=bytes) as fp:
                mp.upload_part_from_file(fp, part_num=i + 1)
        mp.complete_upload()
        should_encode = True

    s3_conn.close()

    if should_encode is True:
        print "Starting encoding job...".format(s3inputfile, src_file)

        zenClient = Zencoder(config.get("zencoder", "key"))
        output_settings = dict(config.items("outputsettings"))
        output_url = "s3://{0}/{1}".format(bucket_name, s3outputfile)
        output_settings.update(dict(url=output_url))

        zenClient.job.create(input="s3://" + bucket_name + "/"+s3inputfile,
                             options={"region": config.get("zencoder", "region")},
                             outputs=[output_settings])


# main


usage = "usage: %prog [options] path_to_source_file_or_directory"
parser = OptionParser(usage)

parser.add_option("-c", "--config", dest="configfile",
                  help="Config file location (defaults to ./encodevideo.ini)", metavar="CONFIGFILE", default="encodevideo.ini")
parser.add_option("-g", "--get-hash", dest="gethash",
                  help="Print S3 filename for the source file (usage: -g true path_to_source_file)", metavar="GETHASH", default=False)
parser.add_option("-t", "--test-target", dest="testtarget",
                  help="Test S3 filename modified date also", metavar="GETHASH", default=False)


(options, args) = parser.parse_args()
if len(args) == 0:
    parser.print_help()
    sys.exit(1)

config = ConfigParser.ConfigParser({"file_extension": ".mp4"})
config.read(options.configfile)

file_arg = args[0]

if not os.path.isfile(file_arg) and not os.path.isdir(file_arg):
    print "ERROR: File or directory \"{0}\" not found or is not readable!".format(file_arg)
    sys.exit(2)

if os.path.isdir(file_arg):
    supported_extensions = json.loads(config.get("application", "allowedextensions"))
    supported_extensions = [x.encode('UTF8') for x in supported_extensions]
    for root, dirs, files in os.walk(file_arg):
        dirs[:] = [d for d in dirs if not d.startswith('.') and not d.startswith('~')]
        files[:] = [f for f in files if not f.startswith('.') and not f.startswith('~')]
        for fn in files:
            if any(fn.lower().endswith(ext) for ext in supported_extensions):
                try:
                    convert_file(os.path.join(root, fn))
                except KeyboardInterrupt:
                    raise
                except:
                    print "Conversion failed for file {0}".format(fn)
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
                    print ''.join('!! ' + line for line in lines)
else:
    try:
        convert_file(file_arg)
    except:
        print "Conversion failed for file {0}".format(file_arg)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        print ''.join('!! ' + line for line in lines)
        sys.exit(255)

sys.exit(0)

